import 'package:ak_car/utils/text_styles.dart';
import 'package:ak_car/views/admin_edit_view.dart';
import 'package:ak_car/views/admin_list_view.dart';
import 'package:ak_car/views/appointments_view.dart';
import 'package:ak_car/views/car_book_view.dart';
import 'package:ak_car/views/car_details_view.dart';
import 'package:ak_car/views/car_edit_view.dart';
import 'package:ak_car/views/car_list_view.dart';
import 'package:ak_car/views/splash_view.dart';
import 'package:ak_car/views/user_login_view.dart';
import 'package:ak_car/views/user_register_view.dart';
import 'package:flutter/material.dart';

const String initialRoute = "login";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SplashView());
      case '/user/login':
        return MaterialPageRoute(builder: (_) => UserLoginView());
      case '/user/register':
        return MaterialPageRoute(builder: (_) => UserRegisterView());
      case '/appointment/list':
        return MaterialPageRoute(builder: (_) => AppointmentsView());
      case '/car/list':
        return MaterialPageRoute(
            settings: RouteSettings(name: '/car/list'),
            builder: (_) => CarListView());
      case '/admin/list':
        return MaterialPageRoute(builder: (_) => AdminsView());
      case '/admin/edit':
        return MaterialPageRoute(builder: (_) => AdminEditView());
      case '/car/add':
        return MaterialPageRoute(builder: (_) => CarEditView());
      case '/car/details':
        return MaterialPageRoute(
            settings: RouteSettings(name: '/car/details'),
            builder: (_) => CarDetailsView(car: settings.arguments));
      case '/car/book':
        return MaterialPageRoute(
            builder: (_) => CarBookView(car: settings.arguments));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  appBar: AppBar(),
                  body: Center(
                    child: Text(
                      'No route defined for ${settings.name}',
                      style: bold(20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ));
    }
  }
}
