import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  static FirebaseUser currentUser;
  static bool isAdmin = false;
  static bool isSuperAdmin = false;
  static Future<AuthResult> registerUser({String email, String password}) =>
      FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);

  static Future<AuthResult> loginUser({String email, String password}) =>
      FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);

  static Future<void> getCurrentUser() async {
    currentUser = await FirebaseAuth.instance.currentUser();
  }

  static Future<void> logUserOut() async {
    currentUser = null;
    isAdmin = false;
    isSuperAdmin = false;
    return FirebaseAuth.instance.signOut();
  }
}
