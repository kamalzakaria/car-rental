import 'package:ak_car/model/base_model.dart';
import 'package:ak_car/services/auth_services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BaseServices {
  static Future<DocumentReference> addDocument(BaseModel model) {
    final db = Firestore.instance.collection(model.collection);
    return db.add(model.toJson());
  }

  static Future<void> updateDocument(BaseModel model) async {
    final db =
        Firestore.instance.collection(model.collection).document(model.docId);
    await db.updateData(model.toJson());
  }

  static Future<void> removeDocument(BaseModel model) async {
    await Firestore.instance
        .collection(model.collection)
        .document(model.docId)
        .delete();
  }

  static Stream<QuerySnapshot> collectionStream(String collection) =>
      Firestore.instance.collection(collection).snapshots();

  static Stream<QuerySnapshot> thisCollectionStream(String collection) =>
      Firestore.instance
          .collection(collection)
          .where('userId', isEqualTo: AuthService.currentUser.uid)
          .snapshots();

  static Stream<DocumentSnapshot> documentStream(BaseModel model) =>
      Firestore.instance
          .collection(model.collection)
          .document(model.docId)
          .snapshots();

  static Future<DocumentSnapshot> document(String collection, String doc) =>
      Firestore.instance.collection(collection).document(doc).snapshots().first;

  static Future<QuerySnapshot> thisDocument(String collection) =>
      Firestore.instance
          .collection(collection)
          .where('email', isEqualTo: AuthService.currentUser.email)
          .snapshots()
          .first;
}
