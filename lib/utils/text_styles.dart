import 'package:flutter/material.dart';

const headerStyle = TextStyle(fontSize: 35, fontWeight: FontWeight.w900);
const subHeaderStyle = TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500);

TextStyle light(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w100,
      decoration: decoration,
    );

TextStyle regular(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w300,
      decoration: decoration,
    );

TextStyle medium(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w500,
      decoration: decoration,
    );

TextStyle bold(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w700,
      decoration: decoration,
    );

TextStyle boldItalic(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      decoration: decoration,
    );

TextStyle blackItalic(
  double size, {
  Color color = Colors.black,
  TextDecoration decoration = TextDecoration.none,
}) =>
    TextStyle(
      fontSize: size,
      color: color,
      decoration: decoration,
    );