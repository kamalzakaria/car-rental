import 'package:flutter/material.dart';

class Spacing extends StatelessWidget {
  final size;
  const Spacing(this.size, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(size / 2),
    );
  }
}
