import 'package:ak_car/utils/spacing.dart';
import 'package:ak_car/utils/text_styles.dart';
import 'package:flutter/material.dart';

class BaseSnackBar {
  static showLoading(GlobalKey<ScaffoldState> scaffoldKey) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.black,
        duration: Duration(seconds: 30),
        content: Row(
          children: <Widget>[
            CircularProgressIndicator(),
            Spacing(20),
            Text('Leks lu...'),
          ],
        )));
  }

  static showError(GlobalKey<ScaffoldState> scaffoldKey,
      {String error = 'Maaf. Ada kesilapan teknikal.'}) {
    scaffoldKey.currentState.hideCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.black,
        content: Row(
          children: <Widget>[
            Icon(Icons.error, color: Colors.red),
            Spacing(20),
            Expanded(
                child: Text(error, style: regular(16, color: Colors.white))),
          ],
        )));
  }

  static showMessage(GlobalKey<ScaffoldState> scaffoldKey,
      {String message = 'Pesanan untuk anda.'}) {
    scaffoldKey.currentState.hideCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.black,
        content: Row(
          children: <Widget>[
            Icon(Icons.info, color: Colors.green),
            Spacing(20),
            Expanded(
                child: Text(message, style: regular(16, color: Colors.white))),
          ],
        )));
  }
}
