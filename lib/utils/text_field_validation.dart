class Validator {
  static String notEmpty(String text) {
    if (text.isEmpty) {
      return '*required';
    } else {
      return null;
    }
  }

  static String email(String text) {
    if (text.isEmpty) {
      return '*required';
    }

    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return regExp.hasMatch(text) ? null : 'Invalid email format';
  }

  static String minLength(String text, int length) {
    if (text.isEmpty) {
      return '*required';
    }
    return text.length < length ? 'Minimum $length character' : null;
  }

  static String maxLength(String text, int length) {
    if (text.isEmpty) {
      return '*required';
    }
    return text.length > length ? 'Maximum $length character only' : null;
  }

  static String exactLength(String text, int length) {
    if (text.isEmpty) {
      return '*required';
    }
    return text.length != length ? 'Invalid format' : null;
  }
}
