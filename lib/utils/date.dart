import 'package:intl/intl.dart';

class DateUtil {
  static String toLocale(DateTime date) => DateFormat('EEE, d MMM, h:mm a').format(date.toLocal());
}