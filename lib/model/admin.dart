import 'package:ak_car/services/collections.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'base_model.dart';

class Admin extends BaseModel {
  final String id;
  final String name;
  final String email;
  final String phone;
  final bool superAdmin;

  Admin({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.superAdmin = false,
  });

  factory Admin.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Admin(
      id: doc.documentID,
      name: data['name'],
      email: data['email'],
      phone: data['phone'],
      superAdmin: data['superAdmin'] ?? false,
    );
  }

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'email': email,
        'phone': phone,
        'superAdmin': superAdmin,
      };

  @override
  String get collection => Collection.admins;
  @override
  String get docId => this.id;
}
