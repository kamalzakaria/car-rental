import 'package:ak_car/model/base_model.dart';
import 'package:ak_car/services/collections.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Car extends BaseModel {
  final String id;
  final String name;
  final String pictureUrl;
  final String color;
  final String location;
  final num ratePerHour;
  final num minimumHours;

  Car({
    this.id,
    this.name,
    this.pictureUrl,
    this.color,
    this.location,
    this.ratePerHour,
    this.minimumHours,
  });

  factory Car.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Car(
      id: doc.documentID,
      name: data['name'],
      pictureUrl: data['pictureUrl'],
      color: data['color'],
      location: data['location'],
      ratePerHour: data['ratePerHour'],
      minimumHours: data['minimumHours'],
    );
  }

  @override
  Map<String, dynamic> toJson() => {
        'name': name,
        'pictureUrl': pictureUrl,
        'color': color,
        'location': location,
        'ratePerHour': ratePerHour,
        'minimumHours': minimumHours,
      };

  @override
  String get collection => Collection.cars;
  @override
  String get docId => this.id;
}
