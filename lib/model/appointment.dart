import 'package:ak_car/services/collections.dart';
import 'package:ak_car/utils/date.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'base_model.dart';

class Appointment extends BaseModel {
  final String id;
  final DateTime startTime;
  final DateTime endTime;
  final num hours;
  final num rate;
  final String carId;
  final String userId;

  Appointment({
    this.id,
    this.startTime,
    this.endTime,
    this.hours,
    this.rate,
    this.carId,
    this.userId,
  });

  factory Appointment.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Appointment(
      id: doc.documentID,
      startTime: DateTime.fromMillisecondsSinceEpoch(
          data["startTime"].millisecondsSinceEpoch),
      endTime: DateTime.fromMillisecondsSinceEpoch(
          data["endTime"].millisecondsSinceEpoch),
      hours: data['hours'],
      rate: data['rate'],
      carId: data['carId'],
      userId: data['userId'],
    );
  }

  @override
  Map<String, dynamic> toJson() => {
        'startTime': startTime,
        'endTime': endTime,
        'hours': hours,
        'rate': rate,
        'carId': carId,
        'userId': userId,
      };

  @override
  String get collection => Collection.appointments;
  @override
  String get docId => this.id;

  String get from => DateUtil.toLocale(startTime);
  String get to => DateUtil.toLocale(endTime);

}
