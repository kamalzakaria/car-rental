import 'package:ak_car/model/car.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:flutter/material.dart';

class CarEditView extends StatefulWidget {
  @override
  _CarEditViewState createState() => _CarEditViewState();
}

class _CarEditViewState extends State<CarEditView> {
  final nameController = TextEditingController(text: 'Proton Saga');
  final colorController = TextEditingController(text: 'Black');
  final locationController = TextEditingController(text: 'Shah Alam');
  final addPictureController = TextEditingController(
      text: 'https://img.rnudah.com/images/92/926827123414717.jpg');
  final rateController = TextEditingController(
      text: '20');
  final minimumController = TextEditingController(
      text: '2');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _onClickSave,
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 40),
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: nameController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Color'),
            controller: colorController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Location'),
            controller: locationController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Add Picture'),
            controller: addPictureController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Rate Per Hour'),
            controller: rateController,
            keyboardType: TextInputType.number,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Minimum Hour'),
            controller: minimumController,
            keyboardType: TextInputType.number,
          ),
        ],
      ),
    );
  }

  void _onClickSave() {
    final car = Car(
      name: nameController.text,
      color: colorController.text,
      location: locationController.text,
      pictureUrl: addPictureController.text,
      ratePerHour: num.parse(rateController.text),
      minimumHours: num.parse(minimumController.text),
    );
    BaseServices.addDocument(car);
    Navigator.pop(context);
  }
}
