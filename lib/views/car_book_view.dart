import 'dart:async';

import 'package:ak_car/model/appointment.dart';
import 'package:ak_car/model/car.dart';
import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/utils/base_snackbar.dart';
import 'package:ak_car/utils/spacing.dart';
import 'package:ak_car/utils/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum DateType { from, to }

class CarBookView extends StatefulWidget {
  final Car car;

  const CarBookView({Key key, this.car}) : super(key: key);
  @override
  _CarBookViewState createState() => _CarBookViewState();
}

class _CarBookViewState extends State<CarBookView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final hoursController = StreamController();
  DateTime from;
  DateTime to;
  num hours;
  num rate;

  @override
  void initState() {
    final now = DateTime.now();
    from = DateTime(now.year, now.month, now.day, (now.hour + 1));
    to = from.add(Duration(hours: 1));
    super.initState();
  }

  @override
  void dispose() {
    hoursController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(),
      body: StreamBuilder<Object>(
          stream: BaseServices.documentStream(widget.car),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());

            return buildBody();
          }),
    );
  }

  Widget buildBody() => Column(
        children: <Widget>[
          Spacing(10),
          Text('From', style: medium(18)),
          Expanded(
              child: CupertinoDatePicker(
            onDateTimeChanged: (date) => _onSelectDate(date, DateType.from),
            initialDateTime: from,
            minimumDate: from,
            minuteInterval: 60,
          )),
          Text('To', style: medium(18)),
          Expanded(
              child: CupertinoDatePicker(
            onDateTimeChanged: (date) => _onSelectDate(date, DateType.to),
            initialDateTime: to,
            minimumDate: to,
            minuteInterval: 60,
          )),
          StreamBuilder<Object>(
              stream: hoursController.stream,
              builder: (context, snapshot) {
                rate = _calculateRate(
                    snapshot.data is String ? 0 : snapshot.data ?? 0);
                return Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text('Total Hours: ${snapshot.data}', style: bold(20)),
                      Text('Total Rate: RM$rate', style: bold(20)),
                    ],
                  ),
                );
              }),
          RaisedButton(onPressed: _onClickBook, child: Text('Book Now'))
        ],
      );

  void _onSelectDate(DateTime date, DateType type) {
    switch (type) {
      case DateType.from:
        from = date;
        break;
      case DateType.to:
        to = date;
        break;
    }
    _calculateHours();
  }

  void _calculateHours() {
    final diff = to.difference(from);
    hours = diff.inHours;
    if (hours < 1) return hoursController.sink.add('Invalid');
    hoursController.sink.add(hours);
  }

  num _calculateRate(num hours) => hours * widget.car.ratePerHour;

  void _onClickBook() async {
    BaseSnackBar.showLoading(_scaffoldKey);
    final appointment = Appointment(
      startTime: from,
      endTime: to,
      hours: hours,
      rate: rate,
      carId: widget.car.id,
      userId: AuthService.currentUser.uid,
    );

    try {
      await BaseServices.addDocument(appointment);
      Navigator.popUntil(context, ModalRoute.withName('/car/list'));
    } catch (e) {
      BaseSnackBar.showError(_scaffoldKey, error: e.message);
    }
  }
}
