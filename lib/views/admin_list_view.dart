import 'package:ak_car/model/admin.dart';
import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/services/collections.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AdminsView extends StatefulWidget {
  @override
  _AdminsViewState createState() => _AdminsViewState();
}

class _AdminsViewState extends State<AdminsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Admins'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: _onPressedAdd,
            )
          ],
        ),
        body: StreamBuilder<QuerySnapshot>(
            stream: BaseServices.collectionStream(Collection.admins),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(child: CircularProgressIndicator());

              final documents = snapshot.data.documents;

              if (documents.isEmpty)
                return Center(child: Text('No Admin Registered'));

              final admins =
                  documents.map((doc) => Admin.fromFirestore(doc)).toList();

              return ListView(
                  children: admins
                      .map((admin) =>
                          admin.superAdmin
                              ? Container()
                              : ListTile(
                                  title: Text(admin.name ?? 'No Name'),
                                  subtitle: Text(admin.email ?? 'No Email'),
                                  trailing: IconButton(
                                      icon: Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                      ),
                                      onPressed: () => _onClickDelete(admin)),
                                ))
                      .toList());
            }));
  }

  void _onPressedAdd() {
    Navigator.pushNamed(context, '/admin/edit');
  }

  void _onClickDelete(Admin admin) {
    BaseServices.removeDocument(admin);
  }
}
