import 'package:ak_car/model/admin.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:flutter/material.dart';

class AdminEditView extends StatefulWidget {
  @override
  _AdminEditViewState createState() => _AdminEditViewState();
}

class _AdminEditViewState extends State<AdminEditView> {
  final nameController = TextEditingController(text: 'Kamal');
  final emailController = TextEditingController(text: 'booluat@yahoo.com');
  final phoneController = TextEditingController(text: '0176663859');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _onClickSave,
          )
        ],
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 40),
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: nameController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            controller: emailController,
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Phone'),
            controller: phoneController,
            keyboardType: TextInputType.number,
          ),
        ],
      ),
    );
  }

  void _onClickSave() {
    final car = Admin(
      name: nameController.text,
      email: emailController.text,
      phone: phoneController.text,
    );
    BaseServices.addDocument(car);
    Navigator.pop(context);
  }
}
