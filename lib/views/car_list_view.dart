import 'package:ak_car/model/car.dart';
import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/services/collections.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CarListView extends StatefulWidget {
  @override
  _CarListViewState createState() => _CarListViewState();
}

class _CarListViewState extends State<CarListView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: AuthService.currentUser == null
            ? IconButton(
                icon: Icon(Icons.account_circle),
                onPressed: () => Navigator.pushNamed(context, '/user/login'),
              )
            : IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  setState(() {
                    AuthService.logUserOut();
                  });
                },
              ),
        actions: <Widget>[
          if (AuthService.isSuperAdmin)
          IconButton(
              icon: Icon(Icons.supervisor_account),
              onPressed: () => Navigator.pushNamed(context, '/admin/list')),
          if (AuthService.isAdmin)
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () => Navigator.pushNamed(context, '/car/add')),
          if (AuthService.currentUser != null)
            IconButton(
                icon: Icon(Icons.watch_later),
                onPressed: () =>
                    Navigator.pushNamed(context, '/appointment/list')),
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: BaseServices.collectionStream(Collection.cars),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());

          final cars = snapshot.data.documents
              .map((doc) => Car.fromFirestore(doc))
              .toList();

          return ListView(
            children: cars
                .map(
                  (car) => ListTile(
                    leading: CircleAvatar(
                        backgroundImage: NetworkImage(car.pictureUrl ?? '')),
                    title: Text(car.name),
                    subtitle: Text(car.color + ' . ' + car.location),
                    trailing: Icon(Icons.navigate_next),
                    onTap: () => Navigator.pushNamed(context, '/car/details',
                        arguments: car),
                  ),
                )
                .toList(),
          );
        },
      ),
    );
  }
}
