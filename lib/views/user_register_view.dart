import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/utils/base_snackbar.dart';
import 'package:ak_car/utils/spacing.dart';
import 'package:flutter/material.dart';

class UserRegisterView extends StatefulWidget {
  @override
  _UserRegisterViewState createState() => _UserRegisterViewState();
}

class _UserRegisterViewState extends State<UserRegisterView> {
  final _email = TextEditingController();
  final _password = TextEditingController();
  final _confirmPassword = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(40),
          children: <Widget>[
            TextFormField(
              controller: _email,
              validator: (text) => text.isEmpty ? 'Error' : null,
              decoration: InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              controller: _password,
              obscureText: true,
              validator: (text) => text.isEmpty ? 'Error' : null,
              decoration: InputDecoration(labelText: 'Password'),
            ),
            TextFormField(
              controller: _confirmPassword,
              obscureText: true,
              validator: (text) => text.isEmpty ? 'Error' : null,
              decoration: InputDecoration(labelText: 'Confirm Password'),
            ),
            Spacing(20),
            RaisedButton(onPressed: _onClickRegister, child: Text('Register')),
          ],
        ),
      ),
    );
  }

  void _onClickRegister() async {
    // Error handling
    if (!_formKey.currentState.validate()) return;
    if (_password.text != _confirmPassword.text)
      return BaseSnackBar.showError(_scaffoldKey,
          error: 'Password don\'t match');

    // Actual function
    BaseSnackBar.showLoading(_scaffoldKey);
    try {
      final result = await AuthService.registerUser(
          email: _email.text, password: _password.text);
      AuthService.currentUser = result.user;
      Navigator.popUntil(context, ModalRoute.withName('/car/details'));
    } catch (e) {
      return BaseSnackBar.showError(_scaffoldKey, error: e.message);
    }
  }
}
