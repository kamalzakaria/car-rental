import 'package:ak_car/model/car.dart';
import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/utils/spacing.dart';
import 'package:ak_car/utils/text_styles.dart';
import 'package:flutter/material.dart';

class CarDetailsView extends StatefulWidget {
  final Car car;

  const CarDetailsView({Key key, this.car}) : super(key: key);
  @override
  _CarDetailsViewState createState() => _CarDetailsViewState();
}

class _CarDetailsViewState extends State<CarDetailsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: StreamBuilder<Object>(
          stream: BaseServices.documentStream(widget.car),
          builder: (context, snapshot) {
            // Loading
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());

            final car = Car.fromFirestore(snapshot.data);

            return Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Image.network(car.pictureUrl),
                      Spacing(10),
                      Text(
                        car.name,
                        style: medium(18),
                        textAlign: TextAlign.center,
                      ),
                      Spacing(10),
                      Text(
                        'RM ${car.ratePerHour}/ hour',
                        style: regular(16),
                        textAlign: TextAlign.center,
                      ),
                      Spacing(10),
                      Text(
                        'Minimum ${car.minimumHours} hours usage',
                        style: regular(16),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                RaisedButton(
                    onPressed: () => Navigator.pushNamed(
                          context,
                          AuthService.currentUser == null ? '/user/login' :
                          '/car/book',
                          arguments: car,
                        ),
                    child: Text(AuthService.currentUser == null ? 'Login/ Sign up to make booking' : 'Pick Date'))
              ],
            );
          }),
    );
  }
}
