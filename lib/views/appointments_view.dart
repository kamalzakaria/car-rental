import 'package:ak_car/model/appointment.dart';
import 'package:ak_car/model/car.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/services/collections.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AppointmentsView extends StatefulWidget {
  @override
  _AppointmentsViewState createState() => _AppointmentsViewState();
}

class _AppointmentsViewState extends State<AppointmentsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Appointments')),
      body: StreamBuilder<QuerySnapshot>(
          stream: BaseServices.thisCollectionStream(Collection.appointments),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(child: CircularProgressIndicator());

            if (snapshot.data.documents.isEmpty)
              return Center(child: Text('No Appointment yet'));

            final appointments = snapshot.data.documents
                .map((doc) => Appointment.fromFirestore(doc))
                .toList();

            return ListView(
                children: appointments
                    .map((appointment) => buildAppointmentTile(appointment))
                    .toList());
          }),
    );
  }

  StreamBuilder buildAppointmentTile(Appointment appointment) {
    // final carSnapshot = BaseServices.document(Collection.cars, carId);
    final car = Car(id: appointment.carId);
    return StreamBuilder<DocumentSnapshot>(
        stream: BaseServices.documentStream(car),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text('fetching...');

          if (snapshot.data.data == null) {
            BaseServices.removeDocument(appointment);
            return Container();
          }

          final car = Car.fromFirestore(snapshot.data);

          return Dismissible(
            key: Key(car.docId),
            background: Container(
              padding: EdgeInsets.all(10),
              color: Colors.red,
              child: Text('Delete'),
              alignment: Alignment.centerRight,
            ),
            direction: DismissDirection.endToStart,
            onDismissed: (direction) => _onDeleteAppointment(appointment),
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(car.pictureUrl),
              ),
              title: Text(car.name),
              subtitle: Text(car.color),
              trailing: Text('${appointment.from}\n${appointment.to}'),
            ),
          );
        });
  }

  void _onDeleteAppointment(Appointment appointment) {
    BaseServices.removeDocument(appointment);
  }
}
