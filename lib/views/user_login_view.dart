import 'package:ak_car/services/auth_services.dart';
import 'package:ak_car/services/base_services.dart';
import 'package:ak_car/services/collections.dart';
import 'package:ak_car/utils/base_snackbar.dart';
import 'package:ak_car/utils/spacing.dart';
import 'package:flutter/material.dart';

class UserLoginView extends StatefulWidget {
  @override
  _UserLoginViewState createState() => _UserLoginViewState();
}

class _UserLoginViewState extends State<UserLoginView> {
  final _email = TextEditingController(text: 'booluat@yahoo.com');
  final _password = TextEditingController(text: '123123');
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(),
      body: Form(
        key: _formKey,
              child: ListView(
          padding: EdgeInsets.all(40),
          children: <Widget>[
            TextFormField(
              controller: _email,
              validator: (text) => text.isEmpty ? 'Error' : null,
              decoration: InputDecoration(labelText: 'Email'),
            ),
            TextFormField(
              controller: _password,
              obscureText: true,
              validator: (text) => text.isEmpty ? 'Error' : null,
              decoration: InputDecoration(labelText: 'Password'),
            ),
            Spacing(20),
            RaisedButton(onPressed: _onClickLogin, child: Text('Login')),
            FlatButton(
                onPressed: _onClickRegister,
                child: Text('Not a member? Click here to register'))
          ],
        ),
      ),
    );
  }

  void _onClickLogin() async {
    // Error handling
    if (!_formKey.currentState.validate()) return;

    // Actual function
    BaseSnackBar.showLoading(_scaffoldKey);
    try {
      final result = await AuthService.loginUser(
          email: _email.text, password: _password.text);
      AuthService.currentUser = result.user;
      final admin = await BaseServices.thisDocument(Collection.admins);
      AuthService.isAdmin = admin.documents.isNotEmpty;
      AuthService.isSuperAdmin = admin.documents.first.data['superAdmin'] ?? false;
      Navigator.pop(context);
    } catch (e) {
      return BaseSnackBar.showError(_scaffoldKey, error: e.message);
    }
  }

  void _onClickRegister() {
    Navigator.pushNamed(context, '/user/register');
  }
}
