import 'package:ak_car/services/auth_services.dart';
import 'package:flutter/material.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    AuthService.getCurrentUser().then((_) => Navigator.pushReplacementNamed(context, '/car/list'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Asyraf Khan Car Rental'),
      ),
    );
  }
}
